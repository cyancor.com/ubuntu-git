FROM ubuntu:latest

ENV DEBIAN_FRONTEND="noninteractive" \
    TERM="xterm"

RUN apt-get update \
    && apt-get install --yes \
        git \
        ssh \
        zip \
        unzip \
        curl \
        wget \
    # Version information
    && mkdir -p /info \
    && cat /etc/os-release | grep VERSION_ID | cut -d "=" -f 2 | cut -d "\"" -f 2 > /info/ubuntu \
    && git --version | cut -d " " -f 3 > /info/git \
    # Cleanup
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*